import React, { Component } from 'react';
import axios from 'axios';

import logo from './logo.svg';
import './App.css';

class Home extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { data: "" };
  }

  fetchDataHandler = () => {
    axios.post('https://jsonplaceholder.typicode.com/posts')
      .then(({ data }) => {
        this.setState({ data });
      })
      .catch((err) => console.log);
  }

  componentDidMount() {
    this.fetchDataHandler();
  }

  render() {
    const { data } = this.state;

    return (
      <code>
        {JSON.stringify(data)}
      </code>
    );
  }
}

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <input type="text" />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn Reacts
        </a>
        <Home />
      </header>
    </div>
  );
}

export default App;
